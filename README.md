# Cross-platform-app: Windows Client

This template/demo shows how to use a  cross-platform business-logic codebase written in C++ in a Windows UWP application.

The template therefore provides 3 different demos:

1. The basic usage of the [C++-library](https://gitlab.com/cross-platform-app/core-template).
2. The rendering of a [C++-based OpenGL (ES) implementation](https://gitlab.com/cross-platform-app/gl-example).
3. An example how to access a System-API, like the device-location via the [C++-library](https://gitlab.com/cross-platform-app/core-template).


## Build Requirements

- Windows SDK >= 10.0.17134.0
- cmake-gui >= 3.7.1 (Download from [here](https://cmake.org/download/) )
- nuget.exe >= 4.6.2 (Download from [here](https://www.nuget.org/downloads) )
- bash.exe (WSL or MINGW) with...
  - jdk <= 1.8 (higher than 1.8 will break the djinni build)

**Important:** Make sure to also meet the dependencies in the submodules `Core` and `gl-example`
 
## Build Instructions
### Initialize submodules
```
git submodule update --init --recursive
```
### Set up CMake-Gui 
1. Before configuration, set the following cache entries, by clicking on the "Add Entry" Button.
    - Name: `CMAKE_SYSTEM_NAME`, Type: `STRING`, Value: `WindowsStore`
    - Name: `CMAKE_SYSTEM_VERSION`, Type: `STRING`, Value: >= `10.0.17134.0`
    - Name: `NUGET_BINARY_DIR`, Type: `PATH`, Value: *Location of nuget.exe*
    - Name: `TARGET_ARCHITECTURE`, Type: `STRING`, Value `Win32`, `x64` or `ARM`, depending on what architecture you are building for.

2. Then click "Configure", "Generate" and "Open Project" to open the generated VS-Project.

![Example setup in CMake GUI](cmake_gui.png)

### Set up Visual Studio Project
1. Set `windowsClient` as **startup project**
2. For each Architecture and Build-type (Debug, Release, ...) you need to run the build twice for the first time.
    - Run build for the first time and try to run the app. It will crash since GLES and EGL libraries are missing in the installation directory.
    - Rebuild `windowsClient` and re-run the application. Now the libraries have been copied to the installation directory correctly and the application should run.

### Explanation of CMake Variables
With `CMAKE_SYSTEM_NAME` and `CMAKE_SYSTEM_VERSION` CMake is told to create a Windows UWP App Project.


`NUGET_BINARY_DIR` is needed to find nuget. You can either search for it's binary in your VS installation, or just download it from  https://www.nuget.org/downloads and then tell CMake where the executable can be found.


`TARGET_ARCHITECTURE` tells CMake, what architecture you want to build for, since CMake is not aware of the architecture during the configuration process.
The Information is used to fetch the right DLL's from the installed Nuget dependencies. In this case, the ANGLE Project is installed. It provides precompiled Versions for x86, x64 and ARM. The correct libraries need to be copied over to the build directory on build time. The script decides which version to choose according to what you set in `TARGET_ARCHITECTURE`.
 

### Explanation of the Visual Studio setup process
The problem is that the libraries that have been installed with nuget have to be copied over to the `AppX` folder manually. This happens in a post-build step, but before the app is launched. On the first launch all dependencies are copied over to `AppX` in the build directory. Since this happens after the post-build step, the folder with the already copied libraries is overridden. This means, that a rebuild has to be triggered, to copy the libraries again. On the next and all future builds everything should be fine.

## Documentation
Generate html documentation with the target `windowsClient--doc`.

The target is only available if a matching version of doxygen could be found on the system (see requirements). 

The output can be found under `doc/html`

## Screenshots
- counter example:
![Counter example](screenshot-1.png)
- _OpenGL ES_ example:
![OpenGL ES example](screenshot-2.png)
- native API example:
![geolocation API example](screenshot-3.png)