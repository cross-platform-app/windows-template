#pragma once

#include "CounterExamplePage.g.h"
#include "core/CounterInterface.hpp"

namespace WindowsClient
{
	/// Page to demonstrate the implementation of the counter from the Logic-Tier
	/**
	 * This implements the user-inteface for the counter from the **Core**.
	 * The counter can be increased up to 10. If the number gets larger than 10, an error message is showing
	 * up after a little timeout.
	 * 
	 * This class has the purpose to demonstrate the following:
	 * - How to interact with objects from the **Core**
	 * - How to make use of the **OnChangeListener** interface.
	 * - How to handle errors with the **OnChangeListener**
	 */
	public ref class CounterExamplePage sealed
	{
	public:
		CounterExamplePage();

		/**
		 * sets **textBlockCounterValue** to the current counter value.
		 * sets the **textBlockCounterError** invisible.
		 */
		void updateCounter();

		/**
		 * enables or disables the **textBlockCounterError** at the bottom of the page.
		 * @param visible	hide or show error message
		 *	- `true`: error message is made visible; 
		 *	- `false`: error message is hidden;
		 */
		void setCounterErrorVisible(bool visible);

		/**
		 * @param canInteract	enable or disable user interaction
		 *	- `true`: buttons to interact with the counter are disabled. The loading indicatior shows up
		 *	- `false`: buttons are enabled. The loading indicator is hidden.
		 */
		void setStateCounterCanInteract(bool canInteract);
	private:
		std::shared_ptr<core::CounterInterface> _counter;
		void onButtonResetCounterClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void onButtonIncreaseCounterClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
	};
}
