#include "UWPLocation.h"


using namespace concurrency;
using namespace Windows::Devices::Geolocation;
using namespace Windows::Foundation;
using namespace Windows::UI::Core;
using namespace Platform;


UWPLocation::UWPLocation() {
	
}

core::LocationRecord UWPLocation::getLocation() {
	if(_position != nullptr) return core::LocationRecord(_position->Coordinate->Latitude, _position->Coordinate->Longitude);
	else return core::LocationRecord(0,0);
}

void UWPLocation::updateLocation(const std::shared_ptr<core::OnChangeListener>& onChangeListener)
{
	try
	{
		task<GeolocationAccessStatus> geolocationAccessRequestTask(Windows::Devices::Geolocation::Geolocator::RequestAccessAsync());

		geolocationAccessRequestTask.then([this, onChangeListener](task<GeolocationAccessStatus> accessStatusTask)
		{
			// Get will throw an exception if the task was canceled or failed with an error
			auto accessStatus = accessStatusTask.get();

			if (accessStatus == GeolocationAccessStatus::Allowed)
			{

				auto geolocator = ref new Windows::Devices::Geolocation::Geolocator();

				task<Geoposition^> geopositionTask(geolocator->GetGeopositionAsync(), _geopositionTaskTokenSource.get_token());
				geopositionTask.then([this, onChangeListener](task<Geoposition^> getPosTask)
				{

					// Get will throw an exception if the task was canceled or failed with an error
					this->_position = getPosTask.get();
					onChangeListener->onSuccess();

				});
			}
			else if (accessStatus == GeolocationAccessStatus::Denied)
			{
				onChangeListener->onError();
			}
			else //GeolocationAccessStatus::Unspecified:
			{
				onChangeListener->onError();
			}
		});
	}
	catch (task_canceled&)
	{
		onChangeListener->onSuccess();
	}
	catch (Exception^ ex)
	{
		onChangeListener->onError();
	}
}
