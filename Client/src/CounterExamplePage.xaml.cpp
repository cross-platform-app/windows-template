#include "pch.h"
#include "CounterExamplePage.xaml.h"
#include <string>
#include "core/OnChangeListener.hpp"

using namespace WindowsClient;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Core;

class OnNumberChangeListener : public core::OnChangeListener {
public:
	OnNumberChangeListener(CounterExamplePage^ page, Windows::UI::Core::CoreDispatcher^ dispatcher) {
		_page = page;
		_dispatcher = dispatcher;
	};
	void onSuccess() {
		_dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this]()
		{
			_page->setStateCounterCanInteract(true);
			_page->updateCounter();
		}));
		
	}
	void onError() {
		_dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this]()
		{
			_page->setStateCounterCanInteract(true);
			_page->setCounterErrorVisible(true);
		}));
		
	}
private:
	CounterExamplePage^ _page;
	Windows::UI::Core::CoreDispatcher^ _dispatcher;
};

CounterExamplePage::CounterExamplePage()
{
	InitializeComponent();
	this->NavigationCacheMode = Windows::UI::Xaml::Navigation::NavigationCacheMode::Required;

	// initialize ExampleClass
	_counter = core::CounterInterface::Counter();
	_counter->onNumberChanged(
		std::shared_ptr<core::OnChangeListener>(new OnNumberChangeListener(this, CoreWindow::GetForCurrentThread()->Dispatcher))
	);
}


void WindowsClient::CounterExamplePage::updateCounter()
{
	textBlockCounterValue->Text = "" + _counter->getNumber();
	setCounterErrorVisible(false);
}

void WindowsClient::CounterExamplePage::setCounterErrorVisible(bool visible)
{
	if(visible) textBlockCounterError->Visibility = Windows::UI::Xaml::Visibility::Visible;
	else textBlockCounterError->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
}

void WindowsClient::CounterExamplePage::setStateCounterCanInteract(bool canInteract)
{
	if (canInteract) {
		buttonIncreaseCounter->IsEnabled = true;
		buttonResetCounter->IsEnabled = true;
		progressRingIncreaseCounter->IsActive = false;
	}
	else {
		buttonIncreaseCounter->IsEnabled = false;
		buttonResetCounter->IsEnabled = false;
		progressRingIncreaseCounter->IsActive = true;
	}
}


void WindowsClient::CounterExamplePage::onButtonResetCounterClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	setStateCounterCanInteract(false);
	Windows::System::Threading::ThreadPool::RunAsync(ref new Windows::System::Threading::WorkItemHandler([this](Windows::Foundation::IAsyncAction ^ action)
	{
		_counter->resetNumber();
	}), Windows::System::Threading::WorkItemPriority::Normal);
}


void WindowsClient::CounterExamplePage::onButtonIncreaseCounterClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	setStateCounterCanInteract(false);
	Windows::System::Threading::ThreadPool::RunAsync(ref new Windows::System::Threading::WorkItemHandler([this](Windows::Foundation::IAsyncAction ^ action)
	{
		_counter->increaseNumber();
	}), Windows::System::Threading::WorkItemPriority::Normal);
}
