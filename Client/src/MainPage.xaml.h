﻿#pragma once

#include "MainPage.g.h"
#include "core/CounterInterface.hpp"

namespace WindowsClient
{
	/// Manages the navigation between the examples
	/**
	 * Contains a **NavigationView** to navigate between the different example pages.
	 */
	public ref class MainPage sealed
	{
	public:
		MainPage();
		virtual ~MainPage();
	private:
		void onNavViewSelectionChanged(Windows::UI::Xaml::Controls::NavigationView^ sender, Windows::UI::Xaml::Controls::NavigationViewSelectionChangedEventArgs^ args);
		void onNavViewLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void navigate(Platform::Object^ item);
	};
}
