﻿//
// App.xaml.h
// Deklaration der App-Klasse
//

#pragma once

#include "App.g.h"
#include "MainPage.xaml.h"

namespace WindowsClient
{
	/// Stellt das anwendungsspezifische Verhalten bereit, um die Standardanwendungsklasse zu ergänzen.
	ref class App sealed
	{
	protected:

		/**
		 * Wird aufgerufen, wenn die Anwendung durch den Endbenutzer normal gestartet wird. 
		 * Weitere Einstiegspunkte werden z. B. verwendet, wenn die Anwendung gestartet wird, um eine bestimmte Datei zu öffnen.
		 * @param e Details über Startanforderung und -prozess.
		 */
		virtual void OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ e) override;

	internal:
		/**
		 * Initialisiert das Singletonanwendungsobjekt. Dies ist die erste Zeile von erstelltem Code
		 * und daher das logische Äquivalent von main() bzw. WinMain().
		 */
		App();

	private:

		MainPage^ mainPage;
	};
}
