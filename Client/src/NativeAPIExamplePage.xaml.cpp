#include "pch.h"
#include "NativeAPIExamplePage.xaml.h"
#include <string>
#include "core/OnChangeListener.hpp"
#include "UWPLocation.h"

using namespace WindowsClient;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;


class OnLocationChangeListener : public core::OnChangeListener {
public:
	OnLocationChangeListener(NativeAPIExamplePage^ page, Windows::UI::Core::CoreDispatcher^ dispatcher) {
		_page = page;
		_dispatcher = dispatcher;
	};
	void onSuccess() {
		_dispatcher->RunAsync(Windows::UI::Core::CoreDispatcherPriority::Normal, ref new Windows::UI::Core::DispatchedHandler([this]()
		{
			_page->showLocationError(false);
			_page->setStateDeviceLocationLoading(false);
			_page->updateLocationDisplay();
		}));
	}
	void onError() {
		_dispatcher->RunAsync(Windows::UI::Core::CoreDispatcherPriority::Normal, ref new Windows::UI::Core::DispatchedHandler([this]()
		{
			_page->setStateDeviceLocationLoading(false);
			_page->showLocationError(true);
		}));
	}
private:
	NativeAPIExamplePage ^ _page;
	Windows::UI::Core::CoreDispatcher^ _dispatcher;
};

NativeAPIExamplePage::NativeAPIExamplePage()
{
	InitializeComponent();
	this->NavigationCacheMode = Windows::UI::Xaml::Navigation::NavigationCacheMode::Enabled;

	_locationService = core::LocationServiceInterface::LocationService(std::shared_ptr<core::LocationInterface>(new UWPLocation));
	_locationService->onLocationChanged(
		std::shared_ptr<core::OnChangeListener>(new OnLocationChangeListener(this, Windows::UI::Core::CoreWindow::GetForCurrentThread()->Dispatcher)));
}

NativeAPIExamplePage::~NativeAPIExamplePage()
{
}

void NativeAPIExamplePage::updateLocationDisplay() {
	auto location = _locationService->getLocation();
	textBlockLatitude->Text = location.lat.ToString();
	textBlockLongitude->Text = location.lon.ToString();
	
}

void WindowsClient::NativeAPIExamplePage::showLocationError(bool show)
{
	if(show) locationDisabledMessage->Visibility = Windows::UI::Xaml::Visibility::Visible;
	else locationDisabledMessage->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
}


void WindowsClient::NativeAPIExamplePage::onButtonUpdateDeviceLocationClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	setStateDeviceLocationLoading(true);
	Windows::System::Threading::ThreadPool::RunAsync(ref new Windows::System::Threading::WorkItemHandler([this](Windows::Foundation::IAsyncAction ^ action)
	{
		_locationService->updateLocation();
	}), Windows::System::Threading::WorkItemPriority::Normal);
	
}

void WindowsClient::NativeAPIExamplePage::setStateDeviceLocationLoading(bool active)
{
	if (active == true) {
		buttonUpdateDeviceLocation->IsEnabled = false;
		progressRingUpdatingDeviceLocation->IsActive = true;
	}
	else {
		progressRingUpdatingDeviceLocation->IsActive = false;
		buttonUpdateDeviceLocation->IsEnabled = true;
	}
}