#pragma once

#include "OpenGLES.h"
#include "OpenGLExamplePage.g.h"

namespace WindowsClient
{
	/// page that displays the **GlExamplePanel** custom panel
	public ref class OpenGLExamplePage sealed
	{
	public:
		OpenGLExamplePage();
		virtual ~OpenGLExamplePage();

	private:
	
	};
}
