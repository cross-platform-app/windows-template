﻿//
// MainPage.xaml.cpp
// Implementierung der MainPage-Klasse
//

#include "pch.h"
#include "MainPage.xaml.h"
#include <string>
#include "CounterExamplePage.xaml.h"
#include "OpenGLExamplePage.xaml.h"
#include "NativeAPIExamplePage.xaml.h"

using namespace WindowsClient;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;



MainPage::MainPage()
{
	InitializeComponent();
	

}

MainPage::~MainPage()
{
}

void MainPage::onNavViewSelectionChanged(Windows::UI::Xaml::Controls::NavigationView^ sender, Windows::UI::Xaml::Controls::NavigationViewSelectionChangedEventArgs^ args)
{
	navigate(args->SelectedItem);
}

void MainPage::navigate(Platform::Object^ item) {
	if (item->Equals(navItemCounter)) {
		contentFrame->Navigate(Windows::UI::Xaml::Interop::TypeName(CounterExamplePage::typeid));
		navView->Header = "Simple Counter Example";

	}
	else if (item->Equals(navItemOpenGL)) {
		contentFrame->Navigate(Windows::UI::Xaml::Interop::TypeName(OpenGLExamplePage::typeid));
		navView->Header = "OpenGL ES Drawing Area";
	}
	else if (item->Equals(navItemAPI)) {
		contentFrame->Navigate(Windows::UI::Xaml::Interop::TypeName(NativeAPIExamplePage::typeid));
		navView->Header = "Calling native GPS API";
	}
}

void MainPage::onNavViewLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	navView->SelectedItem = navItemCounter;
	

}


