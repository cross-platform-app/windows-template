#include "pch.h"
#include "GlExamplePanel.xaml.h"
#include "OpenGLExamplePage.xaml.h"
#include <string>

using namespace WindowsClient;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;




OpenGLExamplePage::OpenGLExamplePage()
{
	InitializeComponent();
	this->NavigationCacheMode = Windows::UI::Xaml::Navigation::NavigationCacheMode::Enabled;
}

OpenGLExamplePage::~OpenGLExamplePage()
{
}

