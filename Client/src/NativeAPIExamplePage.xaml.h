#pragma once

#include "NativeAPIExamplePage.g.h"
#include "core/LocationServiceInterface.hpp"

namespace WindowsClient
{
	/// Page to demonstrate how to expose a System API like GPS to the **Core** and how to consume a Service from the **Core** that uses this API.
	public ref class NativeAPIExamplePage sealed
	{
	public:
		NativeAPIExamplePage();
		virtual ~NativeAPIExamplePage();


		/// updates the TextBlocks that show the current location
		void updateLocationDisplay();

		/**
		 * @param show show location-error at the bottom of the page
		 *	- `true`: error message is visible
		 *	- `false`: error message is not visible
		 */
		void showLocationError(bool show);

		/**
		 * @param active toggle between loading and requesting location.
		 *	- `true`: the button to load the location is disabled; The loading indicator is activated;
		 *	- `false`: the button is enabled; The loading indicator is disabled;
		 */
		void setStateDeviceLocationLoading(bool active);
	private:
		std::shared_ptr<core::LocationServiceInterface> _locationService;
		void onButtonUpdateDeviceLocationClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
	};
}
