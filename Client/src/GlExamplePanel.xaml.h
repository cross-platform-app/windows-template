#pragma once

#include "OpenGLES.h"
#include "glExample\GlExampleInterface.hpp"
#include "GlExamplePanel.g.h"

namespace WindowsClient
{

    /// A SwapChainPanel to display the GlExample Demo
	/**
	 * Starts a Render Loop with the shared _OpenGL ES_ Code from **glExample**.
	 * 
	 * This class has the purpose to demonstrate the following:
	 * - How to use _OpenGL ES_ code on the Universal Windows Platform
	 * - How to start a rendering loop
	 * - How to output _OpenGL ES_ graphics in a **SwapChainPanel**
	 *
	 * Based on this example: https://github.com/Microsoft/angle/tree/ms-master/templates/10/Windows/Universal/XamlUniversal
	 */
	public ref class GlExamplePanel sealed
    {
    public:
        GlExamplePanel();
        virtual ~GlExamplePanel();
    private:
        std::unique_ptr<OpenGLES> mOpenGLES;
		std::shared_ptr<glExample::GlExampleInterface> glExample;


		/// Starts the render loop when the panel is displayed
		void OnPageLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		/// Stops the render loop when the panel is removed
		void OnPageUnloaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		/// handles stop and start of the render loop when the window goes to the background
		void OnVisibilityChanged(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::VisibilityChangedEventArgs^ args);

		void CreateRenderSurface();
		void DestroyRenderSurface();
		void RecoverFromLostDevice();
		void StartRenderLoop();
		void StopRenderLoop();
		

		EGLSurface mRenderSurface;     // This surface is associated with a swapChainPanel on the page
		Concurrency::critical_section mRenderSurfaceCriticalSection;
		Windows::Foundation::IAsyncAction^ mRenderLoopWorker;

		/// stores the current composition scale. Needed for hdpi support
		/// @see CreateRenderSurface()
		float compositionScaleX;

    };
}
