#pragma once

#include "pch.h"
#include "core/LocationInterface.hpp"
#include "core/LocationRecord.hpp"
#include "core/OnChangeListener.hpp"


/// Exposes the Windows UWP Location API to the _Logic Tier_
/**
 * This class is the platform-specific implemenation of **LocationInterface** from the **Core**-Library.
 * It is able to fetch Location-Data from the Windows UWP API and return a LocationRecord, that can then
 * be processed in the **Core**.
 *
 * The Implementation is based on this example: https://github.com/Microsoft/Windows-universal-samples/tree/master/Samples/Geolocation/cpp
 *
 * ## TODO
 * It is not possible yet to find out why an update of the Location failed.
 */
class UWPLocation : public core::LocationInterface {
public:
	UWPLocation();
	
	/**
	 * Call this after updateLocation() has triggered **onSuccess()** on its onChangeListener.
	 * @return last fetched location data. If updateLocation has not been called successfully yet, it returns a (0,0) Location.
	 */
	core::LocationRecord getLocation() override;

	/**
	 * Triggers an asynchronous Location update.
	 * If the update was successful, **onSuccess()** is called in the provided **onChangeListener**.
	 * @param onChangeListener	handles asynchronous callback. **onSuccess()** is called if the Location has been updated.
								**onError()** is called if the update failed for some reason.
	 */
	void updateLocation(const std::shared_ptr<core::OnChangeListener> & onChangeListener) override;
	
private:
	concurrency::cancellation_token_source _geopositionTaskTokenSource;

	Windows::Devices::Geolocation::Geoposition^ _position;
};