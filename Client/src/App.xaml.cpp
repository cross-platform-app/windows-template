﻿//
// App.xaml.cpp
// Implementierung der App-Klasse
//

#include "pch.h"
#include "App.xaml.h"

using namespace WindowsClient;

using namespace Platform;
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;


App::App()
{
    InitializeComponent();
}


void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ e)
{
	mainPage = ref new MainPage();
	Core::CoreApplication::GetCurrentView()->TitleBar->ExtendViewIntoTitleBar = true;
	// Place the page in the current window and ensure that it is active.
	Window::Current->Content = mainPage;
	Window::Current->Activate();

	
}

